<?php

/**
 * @file
 * uw_feed_bulletin_main.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_feed_bulletin_main_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_daily_bulletin';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_daily_bulletin'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_daily_bulletin';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_daily_bulletin'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_daily_bulletin';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_daily_bulletin'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_daily_bulletin';
  $strongarm->value = '1';
  $export['node_preview_daily_bulletin'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_daily_bulletin';
  $strongarm->value = 0;
  $export['node_submitted_daily_bulletin'] = $strongarm;

  return $export;
}
