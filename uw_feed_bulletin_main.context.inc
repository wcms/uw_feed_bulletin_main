<?php

/**
 * @file
 * uw_feed_bulletin_main.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_feed_bulletin_main_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'daily_bulletin_feed_context';
  $context->description = 'Displays bulletin feed on relevant gateway pages.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'gateway/staff' => 'gateway/staff',
        'gateway/faculty' => 'gateway/faculty',
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-daily_bulletin_headlines-block' => array(
          'module' => 'views',
          'delta' => 'daily_bulletin_headlines-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays bulletin feed on relevant gateway pages.');
  $export['daily_bulletin_feed_context'] = $context;

  return $export;
}
