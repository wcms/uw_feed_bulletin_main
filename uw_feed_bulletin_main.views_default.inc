<?php

/**
 * @file
 * uw_feed_bulletin_main.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_feed_bulletin_main_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'daily_bulletin_headlines';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Daily Bulletin Headlines';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<h2>Daily Bulletin</h2>';
  $handler->display->display_options['header']['area']['format'] = 'uw_tf_standard';
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['empty'] = TRUE;
  $handler->display->display_options['footer']['area']['content'] = '<p><a href="http://bulletin.uwaterloo.ca/">See the Daily Bulletin &raquo;</a></p>';
  $handler->display->display_options['footer']['area']['format'] = 'uw_tf_standard';
  /* Field: Content: URL */
  $handler->display->display_options['fields']['field_feed_item_url']['id'] = 'field_feed_item_url';
  $handler->display->display_options['fields']['field_feed_item_url']['table'] = 'field_data_field_feed_item_url';
  $handler->display->display_options['fields']['field_feed_item_url']['field'] = 'field_feed_item_url';
  $handler->display->display_options['fields']['field_feed_item_url']['label'] = '';
  $handler->display->display_options['fields']['field_feed_item_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_feed_item_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_feed_item_url']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_feed_item_url']['click_sort_column'] = 'url';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_feed_item_url]';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Headlines */
  $handler->display->display_options['fields']['field_bulletin_headlines']['id'] = 'field_bulletin_headlines';
  $handler->display->display_options['fields']['field_bulletin_headlines']['table'] = 'field_data_field_bulletin_headlines';
  $handler->display->display_options['fields']['field_bulletin_headlines']['field'] = 'field_bulletin_headlines';
  $handler->display->display_options['fields']['field_bulletin_headlines']['label'] = '';
  $handler->display->display_options['fields']['field_bulletin_headlines']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_bulletin_headlines']['element_class'] = 'bulletin-headlines';
  $handler->display->display_options['fields']['field_bulletin_headlines']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_bulletin_headlines']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_bulletin_headlines']['type'] = 'text_plain';
  $handler->display->display_options['fields']['field_bulletin_headlines']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_bulletin_headlines']['multi_type'] = 'ul';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'daily_bulletin' => 'daily_bulletin',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['daily_bulletin_headlines'] = $view;

  return $export;
}
